use std::time::{
	SystemTime,
	UNIX_EPOCH,
};

use serde::{
	ser::SerializeStruct,
	Deserialize,
	Serialize,
};
use tauri::Manager;

use crate::escape;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum FrontendMessage {
	/// Begin a scan
	Scan {
		/// ID to identify this scan
		scan_id: u32,
		/// Directory to scan, as a list of escaped path components
		directory: String,
	},
	/// Frees resources from a scan. Also cancels it if its running.
	FreeScan { scan_id: u32 },
}

#[derive(Serialize, Clone, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum BackendMessage {
	/// Scan (partial) results
	ScanResult {
		/// ID passed to `Scan`.
		scan_id: u32,

		/// Full escaped canonical path
		dir_canonical: String,
		/// Display-friendly path
		dir_display: String,
		/// Display-friendly path split into components
		dir_components: Vec<CurrentDirComponent>,

		/// Whether the entire directory has been read.
		done: bool,

		/// Directory entries. Keys are escaped names (see [`crate::escape`])
		entries: im::OrdMap<String, DirEntry>,
	},
	/// Scan stopped due to an error
	ScanError {
		/// ID passed to `Scan`.
		scan_id: u32,
		/// Directory entries read so far
		entries: im::OrdMap<String, DirEntry>,
		/// User-friendly error message
		error: String,
	},
	MountsChanged {
		#[serde(serialize_with = "ser_mountpoints")]
		mounts: Vec<mountpoints::MountInfo>,
	},
}

#[derive(Serialize, Clone, Debug)]
pub struct CurrentDirComponent {
	pub display_name: String,
	pub canonical_name: String,
}

#[derive(Serialize, Clone, Debug)]
pub struct DirEntry {
	pub canonical_path: String,

	#[serde(flatten)]
	pub info: DirEntryType,

	#[serde(serialize_with = "ser_systime_to_epoch_ms")]
	pub modified: SystemTime,
	#[serde(serialize_with = "ser_systime_to_epoch_ms")]
	pub created: SystemTime,
	#[serde(serialize_with = "ser_systime_to_epoch_ms")]
	pub accessed: SystemTime,
}

#[derive(Serialize, Clone, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum DirEntryType {
	File {
		size: u64,
		#[serde(flatten)]
		content_type: ContentType,
	},
	Dir,
	Symlink {
		target: String,
		info: Option<Box<DirEntry>>,
	},
	#[cfg_attr(windows, allow(unused))]
	BlockDevice,
	#[cfg_attr(windows, allow(unused))]
	CharDevice,
	#[cfg_attr(windows, allow(unused))]
	Fifo,
	#[cfg_attr(windows, allow(unused))]
	Socket,
	Unknown,
}

#[derive(Serialize, Clone, Debug)]
#[serde(tag = "content_type_status", rename_all = "snake_case")]
pub enum ContentType {
	Pending,
	Unrecognized,
	FromContents {
		#[serde(serialize_with = "ser_mime")]
		content_type: mime::Mime,
	},
	FromExtension {
		#[serde(serialize_with = "ser_mime")]
		content_type: mime::Mime,
	},
}

fn ser_systime_to_epoch_ms<S: serde::Serializer>(
	time: &SystemTime,
	s: S,
) -> Result<S::Ok, S::Error> {
	use serde::ser::Error;
	let dur = time.duration_since(UNIX_EPOCH).map_err(S::Error::custom)?;
	s.serialize_u64(dur.as_millis() as u64)
}

fn ser_mime<S: serde::Serializer>(mime: &mime::Mime, s: S) -> Result<S::Ok, S::Error> {
	s.serialize_str(&mime.to_string())
}

fn ser_mountpoints<S: serde::Serializer>(
	mountpoints: &[mountpoints::MountInfo],
	s: S,
) -> Result<S::Ok, S::Error> {
	struct Wrapper<'a>(&'a mountpoints::MountInfo);
	impl<'a> serde::ser::Serialize for Wrapper<'a> {
		fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where
			S: serde::Serializer,
		{
			let mut s = serializer.serialize_struct("MountInfo", 8)?;
			s.serialize_field(
				"path_canonical",
				&escape::path_to_escaped_string(&self.0.path),
			)?;
			s.serialize_field(
				"path_display",
				&escape::path_to_display_string(&self.0.path),
			)?;
			s.serialize_field("avail", &self.0.avail)?;
			s.serialize_field("free", &self.0.free)?;
			s.serialize_field("size", &self.0.size)?;
			s.serialize_field("name", &self.0.name)?;
			s.serialize_field("format", &self.0.format)?;
			s.serialize_field("readonly", &self.0.readonly)?;
			s.end()
		}
	}

	s.collect_seq(mountpoints.iter().map(Wrapper))
}

pub fn message_sender(
	handle: tauri::AppHandle,
	mut receiver: tokio::sync::mpsc::Receiver<BackendMessage>,
) {
	tauri::async_runtime::spawn(async move {
		while let Some(msg) = receiver.recv().await {
			log::debug!(
				"Backend-to-frontend message: {}",
				serde_json::to_string_pretty(&msg).unwrap()
			);
			handle.emit_all("backend-message", &msg).unwrap();
		}
	});
}
