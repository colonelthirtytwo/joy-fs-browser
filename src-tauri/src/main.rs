#![cfg_attr(
	all(not(debug_assertions), target_os = "windows"),
	windows_subsystem = "windows"
)]

#[macro_use]
extern crate log;

mod escape;
mod interface;
mod mounts;
mod scanner;

use scanner::Scanner;
use tauri::{
	Manager,
	State,
};

#[tauri::command]
async fn frontend_message(
	msg: interface::FrontendMessage,
	scanner: State<'_, Scanner>,
) -> Result<(), ()> {
	log::debug!(
		"Frontend-to-backend message: {}",
		serde_json::to_string_pretty(&msg).unwrap()
	);
	match msg {
		interface::FrontendMessage::Scan { scan_id, directory } => {
			let directory = escape::escaped_str_to_path(&directory)?;
			scanner.begin_scan(scan_id, directory);
		}
		interface::FrontendMessage::FreeScan { scan_id } => {
			scanner.stop_scan(scan_id);
		}
	}
	Ok(())
}

fn main() {
	env_logger::init();

	tauri::Builder::default()
		.invoke_handler(tauri::generate_handler![frontend_message])
		.setup(|app| {
			let (messages_sender, messages_receiver) =
				tokio::sync::mpsc::channel::<crate::interface::BackendMessage>(8);

			let mounts_watch = mounts::watch_mounts().unwrap();
			app.manage(mounts_watch.clone());
			mounts::notify_window_mounts(mounts_watch.clone(), messages_sender.clone());

			let scanner = scanner::Scanner::new(messages_sender.clone(), mounts_watch.clone());
			// DEBUG
			scanner.begin_scan(123, std::env::current_dir().unwrap());
			app.manage(scanner);

			crate::interface::message_sender(app.handle(), messages_receiver);

			Ok(())
		})
		//.register_uri_scheme_protocol("jfsthumb", |app, req| todo!())
		.run(tauri::generate_context!())
		.expect("error while running tauri application");
}
