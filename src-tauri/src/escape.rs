//! OsStr escaping.
//!
//! Paths can be non-utf-8, which makes working with them JS-side a pain.
//! So this module escapes problematic data so we can losslessly pass them to
//! the JS side without a hassle.
//!
//! Escaping differs on Windows and Unix due to how differences in how each handles
//! paths. The scheme is as follows:
//!
//! * On all OSs, `\` is replaced with `\\`.
//! * On Unixes, invalid UTF-8 bytes are replaced with `\xXX`, where `XX` is a two-digit
//!   hex encoding of the byte.
//! * On Windows, invalid UTF-16 sequences are replaced with `\uXXXX`, where `XX` is a
//!   four-digit hex encoding of the code point.

use std::{
	ffi::{
		OsStr,
		OsString,
	},
	fmt::Write,
	path::{
		Path,
		PathBuf,
	},
};

use crate::interface::CurrentDirComponent;

/// Converts an OsStr to a string, escaping any invalid UTF.
#[cfg(unix)]
pub fn osstr_to_escaped_str(s: &OsStr) -> String {
	use std::os::unix::ffi::OsStrExt;
	let mut s = s.as_bytes();
	let mut out = String::new();
	while !s.is_empty() {
		// TODO: need to find backslashes
		match std::str::from_utf8(s) {
			Ok(str) => {
				out.push_str(str);
				break;
			}
			Err(e) => {
				let partial = std::str::from_utf8(&s[..e.valid_up_to()]).unwrap();
				out.push_str(partial);
				s = &s[e.valid_up_to()..];

				let err_len = e.error_len().unwrap_or(s.len());
				for b in &s[..err_len] {
					write!(&mut out, "\\x{:02X}", *b).unwrap();
				}
				s = &s[err_len..];
			}
		}
	}

	out
}

/// Converts an OsStr to a string, escaping any invalid UTF.
#[cfg(windows)]
pub fn osstr_to_escaped_str(s: &OsStr) -> String {
	use std::os::windows::ffi::OsStrExt;
	let mut out = String::new();
	for ch in char::decode_utf16(s.encode_wide()) {
		match ch {
			Ok('\\') => out.push_str(r"\\"),
			Ok(ch) => out.push(ch),
			Err(up) => {
				let up = up.unpaired_surrogate();
				write!(&mut out, "\\u{:04X}", up).unwrap();
			}
		}
	}
	out
}

/// Inverse of [`osstr_to_escaped_str`].
#[cfg(unix)]
pub fn escaped_str_to_osstring(s: &str) -> Result<OsString, ()> {
	use std::os::unix::ffi::OsStringExt;
	let mut enc_buf = [0u8; 8];
	let mut code_buf = String::with_capacity(2);

	let mut out = vec![];
	let mut it = s.chars();
	while let Some(ch) = it.next() {
		match ch {
			'\\' => match it.next() {
				Some('\\') => out.push(b'\\'),
				Some('x') => {
					code_buf.clear();
					for _ in 0..2 {
						code_buf.push(it.next().ok_or(())?);
					}
					let cp = u8::from_str_radix(&code_buf, 16).map_err(|_| ())?;
					out.push(cp);
				}
				None | Some(_) => return Err(()),
			},
			ch => out.extend_from_slice(ch.encode_utf8(&mut enc_buf).as_bytes()),
		}
	}
	Ok(OsString::from_vec(out))
}

/// Inverse of [`osstr_to_escaped_str`].
#[cfg(windows)]
pub fn escaped_str_to_osstring(s: &str) -> Result<OsString, ()> {
	use std::os::windows::ffi::OsStringExt;
	let mut enc_buf = [0u16; 2];
	let mut code_buf = String::with_capacity(4);

	let mut cps = Vec::<u16>::with_capacity(s.len());
	let mut it = s.chars();
	while let Some(ch) = it.next() {
		match ch {
			'\\' => match it.next() {
				Some('\\') => cps.extend_from_slice('\\'.encode_utf16(&mut enc_buf)),
				Some('u') => {
					code_buf.clear();
					for _ in 0..4 {
						code_buf.push(it.next().ok_or(())?);
					}
					let cp = u16::from_str_radix(&code_buf, 16).map_err(|_| ())?;
					cps.push(cp);
				}
				None | Some(_) => return Err(()),
			},
			ch => cps.extend_from_slice(ch.encode_utf16(&mut enc_buf)),
		}
	}
	Ok(OsString::from_wide(&cps))
}

pub fn escaped_str_to_path(s: &str) -> Result<PathBuf, ()> {
	escaped_str_to_osstring(s).map(|s| s.into())
}

pub fn path_to_escaped_string(p: &Path) -> String {
	osstr_to_escaped_str(p.as_os_str())
}

pub fn path_to_display_string(s: &Path) -> String {
	// TODO: windows: strip normalizing prefix
	s.display().to_string()
}

#[cfg(windows)]
pub fn canonical_path_to_display_string_components(s: &Path) -> Vec<CurrentDirComponent> {
	use std::path::Prefix;
	let mut out = vec![];

	// TODO: slice s instead. Doesn't seem to be possible.
	let mut current_path = PathBuf::new();

	let mut components = s.components();
	match components.next().unwrap() {
		Component::Prefix(c) => match c.kind() {
			c @ Prefix::VerbatimUNC(host, share) => {
				let display_str =
					format!(r"\\{}\{}", host.to_string_lossy(), share.to_string_lossy());
				current_path.push(c);
				let canonical_name = path_to_escaped_string(&current_path);
				out.push(CurrentDirComponent {
					display_str,
					canonical_name,
				});
			}
			c @ Prefix::VerbatimDisk(ident_n) => {
				let ch = char::from_u32(ident_n as u32).unwrap_or('?');
				let display_str = format!("{}:", ch);
				current_path.push(c);
				let canonical_name = path_to_escaped_string(&current_path);
				out.push(CurrentDirComponent {
					display_str,
					canonical_name,
				});
			}
			_ => unreachable!(),
		},
		_ => unreachable!(),
	}

	assert!(matches!(components.next().unwrap(), Component::RootDir));

	out.extend(components.map(|comp| {
		current_path.push(&comp);
		let display_name = comp.as_os_str().to_string_lossy().into_owned();
		let canonical_name = path_to_escaped_string(&current_path);
		CurrentDirComponent {
			display_name,
			canonical_name,
		}
	}));

	out
}

#[cfg(unix)]
pub fn canonical_path_to_display_string_components(s: &Path) -> Vec<CurrentDirComponent> {
	// TODO: slice s instead. Doesn't seem to be possible.
	let mut current_path = PathBuf::new();
	s.components()
		.map(|comp| {
			current_path.push(&comp);
			let display_name = comp.as_os_str().to_string_lossy().into_owned();
			let canonical_name = path_to_escaped_string(&current_path);
			CurrentDirComponent {
				display_name,
				canonical_name,
			}
		})
		.collect()
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	pub fn escape_nothing() {
		let inp = OsStr::new(r"totally normal string!");
		assert_eq!(osstr_to_escaped_str(inp), r"totally normal string!");
		assert_eq!(
			escaped_str_to_osstring(&osstr_to_escaped_str(inp)).unwrap(),
			inp
		);
	}

	#[test]
	pub fn escape_windows_normal_path() {
		let inp = OsStr::new(r"C:\foo\bar\baz");
		assert_eq!(osstr_to_escaped_str(inp), r"C:\\foo\\bar\\baz");
		assert_eq!(
			escaped_str_to_osstring(&osstr_to_escaped_str(inp)).unwrap(),
			inp
		);
	}

	#[cfg(windows)]
	#[test]
	pub fn escape_windows_bad_utf16() {
		use std::os::windows::ffi::OsStringExt;
		let inp: OsString =
			OsStringExt::from_wide(&[0x66u16, 0x6f, 0x6f, 0xd801, 0x62, 0x61, 0x72]);
		assert_eq!(osstr_to_escaped_str(&inp), r"foo\uD801bar");
		assert_eq!(
			escaped_str_to_osstring(&osstr_to_escaped_str(&inp)).unwrap(),
			inp
		);
	}
}
