use std::path::Path;

use anyhow::Context;
use tokio::sync::watch;

use crate::interface::BackendMessage;

pub type MountsWatch = watch::Receiver<Vec<mountpoints::MountInfo>>;

pub fn is_sub_path(upper: &Path, lower: &Path) -> bool {
	let mut lower = lower.components();
	let mut upper = upper.components();
	loop {
		match (upper.next(), lower.next()) {
			(None, _) => return true,
			(Some(_), None) => return false,
			(Some(upper_c), Some(lower_c)) => {
				if upper_c != lower_c {
					return false;
				}
			}
		}
	}
}

#[cfg(all(unix, not(macos)))]
pub fn watch_mounts() -> anyhow::Result<MountsWatch> {
	use std::os::unix::prelude::AsRawFd;

	let mut poller = mio::Poll::new()?;
	let mounts =
		std::fs::File::open("/proc/self/mounts").context("While opening /proc/self/mounts")?;
	poller.registry().register(
		&mut mio::unix::SourceFd(&mounts.as_raw_fd()),
		mio::Token(0),
		mio::Interest::READABLE,
	)?;

	let info = mountpoints::mountinfos().unwrap();

	let (send, recv) = watch::channel(info);
	tauri::async_runtime::spawn_blocking(move || {
		// Move file into task so that it doesn't close while being polled
		let _mounts = mounts;
		let mut events = mio::Events::with_capacity(4);
		loop {
			// Wake up occasionally so that we close if all receivers are dropped.
			poller
				.poll(&mut events, Some(std::time::Duration::from_secs(30)))
				.unwrap();
			for ev in events.iter() {
				if ev.is_error() || ev.is_priority() {
					let info = mountpoints::mountinfos().unwrap();
					if send.send(info).is_err() {
						return;
					}
				}
			}
			if send.is_closed() {
				return;
			}
		}
	});
	Ok(recv)
}

// WINDOWS TODO: https://docs.microsoft.com/en-us/windows/win32/devio/detecting-media-insertion-or-removal?redirectedfrom=MSDN
// How to get that if only one callback is allowed and we don't manage it?

#[cfg(windows)]
pub fn watch_mounts() -> anyhow::Result<watch::Receiver<Vec<mountpoints::MountInfo>>> {
	let info = mountpoints::mountinfos().unwrap();
	let (send, recv) = watch::channel(info);
	tauri::async_runtime::spawn_blocking(move || loop {
		std::thread::sleep(std::time::Duration::from_secs(1));
		let info = mountpoints::mountinfos().unwrap();
		if send.send(info).is_err() {
			return;
		}
	});
	Ok(recv)
}

#[cfg(unix)]
pub fn fs_should_open_files(typ: &str) -> bool {
	match typ {
		"vfat" | "ext4" | "ext3" | "ext2" | "ext" | "xfs" | "btrfs" | "zfs" | "exfat"
		| "jfs" | "reisterfs" | "reiser4" | "hfs" | "hfsplus" | "hpfs" | "bcachefs"
		| "fuseblk" /* ntfs, maybe others? */
		=> true,
		_ => false,
	}
}

pub fn notify_window_mounts(
	mut watch: MountsWatch,
	output: tokio::sync::mpsc::Sender<BackendMessage>,
) {
	tauri::async_runtime::spawn(async move {
		loop {
			let mounts = watch.borrow().clone();
			if let Err(_) = output.send(BackendMessage::MountsChanged { mounts }).await {
				return;
			}

			if let Err(_) = watch.changed().await {
				return;
			}
		}
	});
}
