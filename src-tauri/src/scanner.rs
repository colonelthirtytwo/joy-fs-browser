use std::{
	path::{
		Path,
		PathBuf,
	},
	sync::{
		atomic::{
			AtomicBool,
			Ordering,
		},
		Arc,
	},
	time::{
		Duration,
		SystemTime,
	},
};

use dashmap::DashMap;
use tokio::sync::{
	mpsc,
	watch,
};

use crate::{
	escape::{
		self,
		osstr_to_escaped_str,
	},
	interface::{
		BackendMessage,
		DirEntry,
		DirEntryType,
	},
};

const UPDATE_INTERVAL: Duration = Duration::from_millis(500);

pub struct Scanner {
	jobs: DashMap<u32, ScannerTask>,
	mounts: watch::Receiver<Vec<mountpoints::MountInfo>>,
	sender: mpsc::Sender<BackendMessage>,
}
impl Scanner {
	pub fn new(
		messages_sender: mpsc::Sender<BackendMessage>,
		mounts_watch: crate::mounts::MountsWatch,
	) -> Self {
		Self {
			jobs: DashMap::new(),
			mounts: mounts_watch,
			sender: messages_sender,
		}
	}

	pub fn begin_scan(&self, scan_id: u32, directory: PathBuf) {
		debug!("Begin scanning {:?}", directory);
		let cancel_flag = Arc::new(AtomicBool::new(false));
		let jh = tauri::async_runtime::spawn_blocking({
			let cancel_flag = cancel_flag.clone();
			let sender = self.sender.clone();
			move || scan_task(scan_id, directory, cancel_flag, sender)
		});
		self.jobs.insert(
			scan_id,
			ScannerTask {
				_jh: jh,
				cancel_flag,
			},
		);
	}

	pub fn stop_scan(&self, scan_id: u32) {
		if let Some((_, task)) = self.jobs.remove(&scan_id) {
			task.cancel_flag.store(true, Ordering::Relaxed);
		}
	}
}

struct ScannerTask {
	_jh: tauri::async_runtime::JoinHandle<()>,
	cancel_flag: Arc<AtomicBool>,
}

fn scan_task(
	scan_id: u32,
	dir: PathBuf,
	cancel: Arc<AtomicBool>,
	sender: mpsc::Sender<BackendMessage>,
) {
	let mut entries = im::OrdMap::new();
	if let Err(e) = scan_task_inner(scan_id, &dir, cancel, &sender, &mut entries) {
		let _ = sender.blocking_send(BackendMessage::ScanError {
			scan_id,
			entries,
			error: e.to_string(),
		});
	}
}

fn scan_task_inner(
	scan_id: u32,
	dir: &Path,
	cancel: Arc<AtomicBool>,
	sender: &mpsc::Sender<BackendMessage>,
	entries: &mut im::OrdMap<String, DirEntry>,
) -> Result<(), std::io::Error> {
	let dir = std::fs::canonicalize(dir)?;

	let dir_canonical = escape::path_to_escaped_string(&dir);
	let dir_display = escape::path_to_display_string(&dir);
	let dir_components = escape::canonical_path_to_display_string_components(&dir);

	let mut readdir = std::fs::read_dir(dir)?;
	let mut next_broadcast_time = std::time::Instant::now() + UPDATE_INTERVAL;

	while let Some(entry) = readdir.next() {
		let entry = entry?;
		if cancel.load(Ordering::Relaxed) {
			return Ok(());
		}

		let canonical_path = escape::path_to_escaped_string(&entry.path());

		let name = osstr_to_escaped_str(&entry.file_name());
		let md = entry.metadata()?;
		let accessed = md.accessed().unwrap_or(SystemTime::UNIX_EPOCH);
		let modified = md.modified().unwrap_or(SystemTime::UNIX_EPOCH);
		let created = md.created().unwrap_or(SystemTime::UNIX_EPOCH);
		let info = file_info(&entry, &md)?;

		entries.insert(
			name,
			DirEntry {
				canonical_path,
				modified,
				created,
				accessed,
				info,
			},
		);

		let now = std::time::Instant::now();
		if now >= next_broadcast_time {
			next_broadcast_time = now + UPDATE_INTERVAL;
			if let Err(_) = sender.blocking_send(BackendMessage::ScanResult {
				scan_id,
				dir_canonical: dir_canonical.clone(),
				dir_display: dir_display.clone(),
				dir_components: dir_components.clone(),
				entries: entries.clone(),
				done: false,
			}) {
				// channel shutdown, no use in continuing
				return Ok(());
			}
		}
	}
	let _ = sender.blocking_send(BackendMessage::ScanResult {
		scan_id,
		dir_canonical: dir_canonical.clone(),
		dir_display: dir_display.clone(),
		dir_components: dir_components.clone(),
		entries: entries.clone(),
		done: true,
	});
	Ok(())
}

fn file_info(
	entry: &std::fs::DirEntry,
	md: &std::fs::Metadata,
) -> Result<DirEntryType, std::io::Error> {
	#[cfg(unix)]
	use std::os::unix::prelude::FileTypeExt;

	let ty = md.file_type();
	if ty.is_dir() {
		return Ok(DirEntryType::Dir);
	}
	if ty.is_file() {
		return Ok(DirEntryType::File {
			size: md.len(),
			content_type: crate::interface::ContentType::Pending,
		});
	}
	if ty.is_symlink() {
		let target = std::fs::read_link(&entry.path())?;
		return Ok(DirEntryType::Symlink {
			target: escape::path_to_escaped_string(&target),
			// TODO: info
			info: None,
		});
	}

	#[cfg(unix)]
	if ty.is_char_device() {
		return Ok(DirEntryType::CharDevice);
	}
	#[cfg(unix)]
	if ty.is_block_device() {
		return Ok(DirEntryType::BlockDevice);
	}
	#[cfg(unix)]
	if ty.is_fifo() {
		return Ok(DirEntryType::Fifo);
	}
	#[cfg(unix)]
	if ty.is_socket() {
		return Ok(DirEntryType::Socket);
	}

	Ok(DirEntryType::Unknown)
}
