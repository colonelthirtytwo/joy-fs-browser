use std::{path::Path, io::{BufRead, Read, Seek, SeekFrom}};

use anyhow::Context;
use thumbnailer::error::ThumbError;

use crate::interface::ContentType;


pub fn identify<R: Read>(path: &Path, f: &mut R) -> anyhow::Result<ContentType> {
	let mut f = std::io::BufReader::new(f);
	let mut buf = [0u8; 8192];
	let num = f.read(&mut buf).context("While reading file")?;
	let buf = &buf[..num];

	if let Some(v) = infer::get(buf) {
		return Ok(ContentType::FromContents { content_type: v.mime_type().parse().unwrap() })
	}

	if let Some(t) = mime_guess::from_path(path).first() {
		return Ok(ContentType::FromExtension { content_type: t });
	}

	Ok(ContentType::Unrecognized)
}

pub fn thumbnail<R: BufRead + Seek>(f: &mut R, ct: &ContentType) -> anyhow::Result<Option<thumbnailer::Thumbnail>> {
	let mime = match ct {
		ContentType::FromContents { content_type } => content_type,
		ContentType::FromExtension { content_type } => content_type,
		_ => return Ok(None),
	};

	match thumbnailer::create_thumbnails(f, mime.clone(), [thumbnailer::ThumbnailSize::Custom((256,256))]) {
		Ok(img) => Ok(Some(img.into_iter().next().unwrap())),
		Err(ThumbError::Unsupported(_)) => Ok(None),
		Err(e) => Err(anyhow::anyhow!(e)),
	}
}
