import 'gamecontroller.js'

import { useThrottleFn } from 'ahooks'
import { useEffect } from 'react'

export enum GamepadButton {
  'Up' = 12,
  'Down',
  'Left',
  'Right',
}

export class GamepadButtonEvent extends Event {
  button: string

  constructor(button: number) {
    super('gamepadbutton')

    this.button = GamepadButton[button]
  }
}

const listenerButtons = [12, 13, 14, 15]

export default function useGamepads() {
  const { run, cancel, flush } = useThrottleFn(
    (button) => {
      window.dispatchEvent(new GamepadButtonEvent(button))
    },
    { leading: true, trailing: false, wait: 100 }
  )

  useEffect(() => {
    window.gameControl.on('connect', (gamepad) => {
      for (const id of listenerButtons) {
        gamepad.on(`button${id}`, () => {
          run(id)
        })
      }
    })

    return () => {
      window.gameControl.off('connect')
    }
  }, [cancel, run])
}
