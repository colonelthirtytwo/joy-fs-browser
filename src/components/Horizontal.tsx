import c from 'classnames'
import { Children } from 'react'

import { FocusItem, useHorizontalList } from '../FocusContext'

export default function Horizontal({ children }) {
  const { active, parentActive, index, navigate } = useHorizontalList(children.length)

  return (
    <div
      className={c(
        'flex flex-row flex-1 p-2 gap-2 bg-gray-500 transtion-colors duration-150',
        active ? 'bg-opacity-10' : 'bg-opacity-5'
      )}
    >
      {Children.map(children, (child, i) => (
        <FocusItem
          key={i}
          parentActive={parentActive}
          active={active && index === i}
          navigate={navigate}
        >
          {child}
        </FocusItem>
      ))}
    </div>
  )
}
