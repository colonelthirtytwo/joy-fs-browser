import c from 'classnames'

import { FocusItem, useFocusNavigate, useVerticalList } from '../FocusContext'

function File({ text }) {
  const active = useFocusNavigate()

  return (
    <div
      className={c(
        'File p-4 rounded-md bg-gray-500 transition-colors duration-150',
        active ? 'bg-opacity-50' : 'bg-opacity-10'
      )}
    >
      <div className={c('transition-transform', active ? 'translate-x-2' : '')}>{text}</div>
    </div>
  )
}

export default function Files({ files = [] }) {
  const { active, index, navigate } = useVerticalList(files.length)

  return (
    <div
      className={c('Files flex flex-col p-2 gap-2 transition-opacity', { 'opacity-20': !active })}
    >
      {files.map((child, i) => (
        <FocusItem key={i} parentActive={active} active={index === i} navigate={navigate}>
          <File text={child} />
        </FocusItem>
      ))}
    </div>
  )
}
