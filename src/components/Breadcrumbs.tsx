import c from 'classnames'
import { useMemo } from 'react'

import { FocusItem, useFocusNavigate, useHorizontalList } from '../FocusContext'

function Breadcrumb({ text }) {
  const active = useFocusNavigate()

  return (
    <div
      className={c(
        'Breadcrumb px-4 py-2 rounded-md bg-gray-500 transtion-colors duration-150',
        active ? 'bg-opacity-50' : 'bg-opacity-10'
      )}
    >
      {text}
    </div>
  )
}

export default function Breadcrumbs({ path }) {
  const pathParts = useMemo(() => path.split('/'), [path])

  const { active, index, navigate } = useHorizontalList(pathParts.length)

  return (
    <div
      className={c('Breadcrumbs flex flex-row p-2 gap-4 transition-opacity duration-300', {
        'opacity-20': !active,
      })}
    >
      {pathParts.map((child, i) => (
        <FocusItem key={i} parentActive={active} active={index === i} navigate={navigate}>
          <Breadcrumb text={child} />
        </FocusItem>
      ))}
    </div>
  )
}
