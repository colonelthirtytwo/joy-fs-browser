import { createContext, useCallback, useContext, useEffect, useState } from 'react'

import { GamepadButtonEvent } from './hooks/useGamepads'

type NavigationCallback = (key: string, index?: number) => void

const FocusContext = createContext({
  parentActive: true,
  active: true,
  navigate: (key: string, index?: number) => {},
})

const useFocusContext = () => useContext(FocusContext)
export default useFocusContext

export const Provider = FocusContext.Provider

export const useFocusNavigate = () => {
  const { active, parentActive, navigate } = useContext(FocusContext)

  useEffect(() => {
    function gamepadButton(ev: GamepadButtonEvent) {
      navigate(ev.button)
    }

    if (active && parentActive) {
      window.addEventListener('gamepadbutton', gamepadButton)

      return () => window.removeEventListener('gamepadbutton', gamepadButton)
    }
  }, [active, navigate, parentActive])

  return active
}

export const useVerticalList = (children) => {
  const { active, parentActive, navigate: parentNavigate } = useFocusContext()

  const [index, setIndex] = useState(0)

  const navigate = useCallback<NavigationCallback>(
    (key, childIndex) => {
      if (key === 'Left' || key === 'Right') return parentNavigate(key)

      if (key === 'Up') {
        if (index === 0) {
          parentNavigate(key, index)
        } else {
          setIndex(index - 1)
        }
      }

      if (key === 'Down') {
        if (index === children - 1) {
          parentNavigate(key, index)
        } else {
          setIndex(index + 1)
        }
      }
    },
    [children, index, parentNavigate]
  )

  return { active, parentActive, index, navigate }
}

export const useHorizontalList = (children) => {
  const { active, parentActive, navigate: parentNavigate } = useFocusContext()

  const [index, setIndex] = useState(0)

  const navigate = useCallback(
    (key, childIndex) => {
      if (key === 'Up' || key === 'Down') return parentNavigate(key)

      if (key === 'Left') {
        if (index === 0) {
          parentNavigate(key, index)
        } else {
          setIndex(index - 1)
        }
      }

      if (key === 'Right') {
        if (index === children - 1) {
          parentNavigate(key, index)
        } else {
          setIndex(index + 1)
        }
      }
    },
    [children, index, parentNavigate]
  )

  return { active, parentActive, index, navigate }
}

export function FocusItem({ parentActive, active, navigate, children }) {
  return <Provider value={{ parentActive, active, navigate }}>{children}</Provider>
}
