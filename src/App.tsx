import Breadcrumbs from './components/Breadcrumbs'
import Files from './components/Files'
import Horizontal from './components/Horizontal'
import Vertical from './components/Vertical'
import useGamepads from './hooks/useGamepads'

export default function App() {
  useGamepads()

  return (
    <div className="flex flex-col w-screen h-screen text-white bg-gray-900">
      <Vertical>
        <Horizontal>
          <Vertical>
            <Breadcrumbs path="foo/bar/baz" />
            <Files files={['foo', 'bar', 'baz']} />
          </Vertical>
          <Vertical>
            <Breadcrumbs path="foo/bar/baz" />
            <Files files={['foo', 'bar', 'baz']} />
          </Vertical>
        </Horizontal>
        <Horizontal>
          <Vertical>
            <Breadcrumbs path="foo/bar/baz" />
            <Files files={['foo', 'bar', 'baz']} />
          </Vertical>
          <Vertical>
            <Breadcrumbs path="foo/bar/baz" />
            <Files files={['foo', 'bar', 'baz']} />
          </Vertical>
        </Horizontal>
      </Vertical>
    </div>
  )
}
